# Instrunciones 📄

El proposito de este documento es explicar el funcionamiento del  **_Edelvives_** test.

## Requerimientos ⚙️
- Unix based OS
- Ruby 2.7.0
- Rails 6.1.6
## Pasos 🌀

1. Ejecutar git clone https://gitlab.com/leito_18/test_edelvives.git para bajar el proyecto en local.
2. Ejecutar `cd test_edelvives` para ir al root del proyecto.
3. Ejecutar `rails db:seed` para poblar nuestra base de datos de información
4. Ejecutar `bundle install` para instalar dependencias
5. Ejecutar `rails s` para levantar el servidor.
6. En el navegado nos vamos a `localhost:3000` para ver nuestra app corriendo.
7. La api solicitada esta en la ruta `http://localhost:3000/api/v1/people/1` mostrara la información por persona.

## Nota 📋
Los scripts solicitados de la pregunta 1 y 2 se encuentran en la ruta app/scripts

## Descripción 📋
Crear un mini proyecto ruby on rails que permita hacer una llamada a una API para obtener un JSON que devuelva los comunicados de las personas mayores de edad, y el número de adjuntos total de los comunicados, incluido el cero. A parte dos de dos scripts.


## Mejoras 🚩️
- Los test de la api no fueron realizados.
- Refactorizar más los metodos de los scripts

