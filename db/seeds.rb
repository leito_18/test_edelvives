# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Person.create(name: "Lucia", birth_day: "2000-02-13")
Person.create(name: "Rosa", birth_day: "2010-05-24")
Person.create(name: "Maria", birth_day: "2001-04-08")

Comunicating.create(creator_id: 11, receiver_id: 1, subject: "Lorem Ipsum 5", content: "Esto es un contenido 5", comunicating_previus_id: 1)
Comunicating.create(creator_id: 13, receiver_id: 1, subject: "Lorem Ipsum 6", content: "Esto es un contenido 6", comunicating_previus_id: 1)
Comunicating.create(creator_id: 14, receiver_id: 1, subject: "Lorem Ipsum 7", content: "Esto es un contenido 8", comunicating_previus_id: 1)

Attachment.create(comunicating_id: 4)
Attachment.create(comunicating_id: 4)
Attachment.create(comunicating_id: 5)

