class AddColumnAttachmentCountToComunicatings < ActiveRecord::Migration[6.1]
  def change
    add_column :comunicatings, :attachment_count, :integer
  end
end
