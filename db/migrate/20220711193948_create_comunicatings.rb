class CreateComunicatings < ActiveRecord::Migration[6.1]
  def change
    create_table :comunicatings do |t|
      t.integer :creator_id
      t.integer :receiver_id
      t.string :subject
      t.string :content
      t.integer :comunicating_previus_id

      t.timestamps
    end
  end
end
