class AddIndexCreatorIdToComunicatings < ActiveRecord::Migration[6.1]
  def change
    add_index :comunicatings, :creator_id
  end
end
