Rails.application.routes.draw do

  resources :attachments
  resources :attacheds
  resources :comunicatings
  namespace :api do
    namespace :v1 do
      resources :people
      get '/comunicados', to: 'people#obtener_comunicados'
    end
  end  
  
  # /api/v1/people
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
