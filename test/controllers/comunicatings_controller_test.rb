require "test_helper"

class ComunicatingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comunicating = comunicatings(:one)
  end

  test "should get index" do
    get comunicatings_url, as: :json
    assert_response :success
  end

  test "should create comunicating" do
    assert_difference('Comunicating.count') do
      post comunicatings_url, params: { comunicating: { comunicating_previus_id: @comunicating.comunicating_previus_id, content: @comunicating.content, creator_id: @comunicating.creator_id, integer: @comunicating.integer, receiver_id: @comunicating.receiver_id, string: @comunicating.string, subject: @comunicating.subject } }, as: :json
    end

    assert_response 201
  end

  test "should show comunicating" do
    get comunicating_url(@comunicating), as: :json
    assert_response :success
  end

  test "should update comunicating" do
    patch comunicating_url(@comunicating), params: { comunicating: { comunicating_previus_id: @comunicating.comunicating_previus_id, content: @comunicating.content, creator_id: @comunicating.creator_id, integer: @comunicating.integer, receiver_id: @comunicating.receiver_id, string: @comunicating.string, subject: @comunicating.subject } }, as: :json
    assert_response 200
  end

  test "should destroy comunicating" do
    assert_difference('Comunicating.count', -1) do
      delete comunicating_url(@comunicating), as: :json
    end

    assert_response 204
  end
end
