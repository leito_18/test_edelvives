class Api::V1::PeopleController < ApplicationController
  before_action :set_person, only: [:show, :update, :destroy, :obtener_comunicados]

  # GET /people
  def index
    @people = Person.all
    render json: @people
  end

  # GET /people/1
  def show
    @comunicados_x_persona = Comunicating.joins("INNER JOIN people ON people.id = comunicatings.creator_id")
                             .where("people.id = '#{params[:id]}'").order(created_at: :asc)
    
    @comunicados_x_persona.each do |c|
      @attached_x_comunication = Attachment.where(comunicating_id: c.id).size
      c['attachment_count'] = @attached_x_comunication.to_i
    end

    if get_old_age(params[:id])
      render json: @comunicados_x_persona
    else
      render json: {"message": "Es menor de edad"} 
    end
    
  end

  # POST /people
  def create
    @person = Person.new(person_params)

    if @person.save
      render json: @person, status: :created, location: api_v1_people_url(@person)
    else
      render json: @person.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /people/1
  def update
    if @person.update(person_params)
      render json: @person
    else
      render json: @person.errors, status: :unprocessable_entity
    end
  end

  # DELETE /people/1
  def destroy
    @person.destroy
  end

  
  def get_old_age(id)
    current_year = Time.now.year
    @people = Person.find(id)
      year_date_person = @people.birth_day.strftime('%Y')
      age = current_year.to_i - year_date_person.to_i
      if age > 18
        return true
      else
        return false
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def person_params
      params.require(:person).permit(:id, :name, :birth_day)
    end
end
