class ComunicatingsController < ApplicationController
  before_action :set_comunicating, only: [:show, :update, :destroy]

  # GET /comunicatings
  def index
    @comunicatings = Comunicating.all

    render json: @comunicatings
  end

  # GET /comunicatings/1
  def show
    render json: @comunicating
  end

  # POST /comunicatings
  def create
    @comunicating = Comunicating.new(comunicating_params)

    if @comunicating.save
      render json: @comunicating, status: :created, location: @comunicating
    else
      render json: @comunicating.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /comunicatings/1
  def update
    if @comunicating.update(comunicating_params)
      render json: @comunicating
    else
      render json: @comunicating.errors, status: :unprocessable_entity
    end
  end

  # DELETE /comunicatings/1
  def destroy
    @comunicating.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comunicating
      @comunicating = Comunicating.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def comunicating_params
      params.require(:comunicating).permit(:creator_id, :integer, :receiver_id, :integer, :subject, :string, :content, :string, :comunicating_previus_id, :integer)
    end
end
