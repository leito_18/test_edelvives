def chunk_array(array, number)
    divisible = array.length % number == 0
    size = (array.size/number.to_f).round
    output = []
  
    if !divisible
      array = make_divisible(array, number, size)
      size = (array.size/number.to_f).round
    end
    
    array.each_slice(size) do |array|
     if output.length < number
       output << array
     end
    end
    
    print output
  end
  
  def make_divisible(array, number, size)
    needed = (size - 1) - (array.length % number)
  
    needed.abs.times do
      array << nil
    end
  
    array
  end
  
  a = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l']
  
  chunk_array(a, 3)