def suma_factorial(num)
    array = [*1..num]
    acum = 0
  
    array.reverse.each_with_index do |el, index|
      new_arr  = [*1..el]
      acum    += new_arr.sum
      sequence = new_arr.join(" + ")
  
      output = if index != array.length - 1
                 "\n #{sequence} +"
               else
                 "\n #{sequence}"
               end
               
      puts output
    end
    puts "\n\n Total: #{acum} "
end
  
suma_factorial(10)